
# Documentação de Utilização

(Copiado de <https://github.com/fernandesjm/PreservationServiceProofOfConcept>)

Esta secção consiste num conjunto de tutorias, sendo assim uma espécie de manual de instruções, permitindo que o utilizador tenha um acesso facilitado a todas as informações necessárias para o uso da Prova de Conceito, aumentando assim as probabilidades de existir um uso correto da mesma.

## Pré Requisitos de Instalação e de Utilização

- Java 13 ou superior;
- Maven 3.6 ou superior;
- MongoDB instalado e a correr;
- RODA instalado numa máquina à escolha e a correr.

  Para a instalação e *deployment* do ambiente de teste do repositório digital RODA, consultar <https://github.com/eark-project/roda/blob/master/documentation/Installation_Testing_Environments.md>

## Instalação

Após a clonagem do repositório e da instalação do RODA, uma viagem até à diretoria *"Preservation-Service/src/main/java/com/devisefutures/projeto/"*  para fazer uma alteração no ficheiro *Roda.java*, responsável pela comunicação com o repositório RODA.

A *String baseUrl* cujo valor *"XXX.XXX.XXX.XXX",*  deve ser substituído pelo endereço onde o repositório RODA se encontra instalado. Caso esteja instalado na mesma máquina que o resto do sistema da Prova de Conceito, "*localhost*" deverá ser utilizado.

## Demo

Para a realização de uma *demo* desta Prova de Conceito, dois pares de chaves foram pedidos, tal como foi abordado na secção 3.5.

Um deles para o Serviço de Preservação usar para criar as evidências, com o certificado em nome de "*presservproofofconcept@gmail.com*".

Para a criação das evidÊncias, precisamos ainda de especificar o tipo de *KeyStore* a utilizar para assinar, mais simplesmente, onde a chave privada pode ser encontrada.

Estes pares de chaves, pelo menos o do serviço de preservação, deveriam estar protegidas por um dispositivo de criação de assinaturas qualificadas.

Posto isto foi desenvolvida uma classe em Java *Pkc12.java* que age como uma *PKC12 Key Store*.

O outro foi para um *Mock Client*, para a criação de assinaturas teste, para a submissão ao sistema de preservação., com o certificado em nome de "*mock.user.sign@gmail.com*".

Nesta Prova de Conceito estão localizados em *Preservation-Service/src/main/java/com/devisefutures/resources/keystore/*.

Com o intuito da concretização de testes, foi desenvolvida uma classe Java \textit{GenSignatureToPreserve.java} onde é possível a criação de assinaturas eletrónicas avançadas de todos os formatos (XadES, CadES e PadES, recorrendo ao par de chaves do *Mock Client*.

Sendo que podem ser submetidas ao sistema da Prova de conceito outro tipo de assinaturas, como assinaturas eletrónicas qualificadas.

## Modo de Utilização

Uma vez que os pré requisitos sejam cumpridos e a instalação esteja feita, o ficheiro *inicialInterface.java* deve ser corrido para fazer o *deploy* da aplicação.

Este ficheiro encontra-se em *Preservation-Service/src/main/java/com/devisefutures/projeto/gui/*

- **Utilizador (Cliente)**
  O Cliente pode fazer uso da Prova de Conceito tal como descrito na secção 3.7.2.
- **Administrador**
  O Administrador abre o seu *browser* de eleição e dirige-se ao endereço onde instalou o repositório na porta *8080*, acedendo à *Interface de Administração*. Faz o login com as credenciais *admin roda*. Tal como indicado na secção 3.7.2 no caso de uso nº6.

Material exemplo criado pelo sistema da Prova de Conceito pode ser encontrado na pasta resources, dentro da pasta "EXAMPLE OUT Material"

##### Para obter mais informações ou uma cópia  do relatório da dissertação de Mestrado sobre o Serviço do Preservação - eIDAS Qualified Trust Services é favor de entrar em contacto para :  pg38930@alunos.uminho.pt
