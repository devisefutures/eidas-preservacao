/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: ASiCXSAdESSignLTALevel
* Class Constructor: ASiCXSAdESSignLTALevel(ist<DSSDocument> toPreserve)
* This class receives as argument a list of DSSDocuments that will be the contents of the ASiC container
*
*
* This class aims to build an ASiC Containers with associated XadES-LTA advanced signature
* using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/

package com.devisefutures.projeto;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.io.File;

import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.asic.xades.ASiCWithXAdESSignatureParameters;
import eu.europa.esig.dss.asic.xades.signature.ASiCWithXAdESService;

import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;

import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.enumerations.ASiCContainerType;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.model.ToBeSigned;

import eu.europa.esig.dss.service.http.commons.OCSPDataLoader;
import eu.europa.esig.dss.tsl.source.LOTLSource;
import eu.europa.esig.dss.service.http.commons.FileCacheDataLoader;
import eu.europa.esig.dss.tsl.cache.CacheCleaner;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;

import eu.europa.esig.dss.spi.x509.CommonCertificateSource;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.Policy;
import eu.europa.esig.dss.spi.DSSUtils;

public class ASiCSXAdESSignLTALevel {
	Pkc12 pk;
	private CommonCertificateVerifier commonCertificateVerifier;
	DSSDocument signedDocument;

	public ASiCSXAdESSignLTALevel(List<DSSDocument> toPreserve) throws IOException, URISyntaxException {
		// Instantiate the Preservation Service Pkc12 keystore object
		pk = new Pkc12(0);
		
		// Preparing parameters for the ASiCWithXAdES signature
		ASiCWithXAdESSignatureParameters signatureParameters = new ASiCWithXAdESSignatureParameters();
		// We define the level of the signature (-B, -T, -LT, -LTA)
		signatureParameters.setSignatureLevel(SignatureLevel.XAdES_BASELINE_LTA);
		// We choose the container type (ASiC-S or ASiC-E)
		signatureParameters.aSiC().setContainerType(ASiCContainerType.ASiC_S);
		// We set the digest algorithm to use with the signature algorithm
		signatureParameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
		// We get the signing certificate from the PKC12 keystore
		signatureParameters.setSigningCertificate(pk.entry.getCertificate());
		// We get the certificate chain from the PKC12 keystore
		signatureParameters.setCertificateChain(pk.entry.getCertificateChain());
		// We set the date of the signature
		signatureParameters.bLevel().setSigningDate(new Date());


		// Create common certificate verifier
		commonCertificateVerifier = new CommonCertificateVerifier();

		// Get and use the explicit policy
		String signaturePolicyId = "https://www.devisefutures.com/pdf/preservacao/PoliticaPreservacao.pdf";
		DigestAlgorithm signaturePolicyHashAlgo = DigestAlgorithm.SHA256;

		// We digest the policy document
		byte[] signaturePolicyDescriptionBytes = getClass().getResourceAsStream("/PoliticaPreservacao.pdf").readAllBytes();
		byte[] digestedBytes = DSSUtils.digest(signaturePolicyHashAlgo, signaturePolicyDescriptionBytes);
		
		// Instantiate a Policy object
		Policy policy = new Policy();
		//The string representation of the OID of the signature policy to use when signing
		policy.setId(signaturePolicyId);
		// The hash function used to compute the value of the SignaturePolicyHashValue entry.
		policy.setDigestAlgorithm(signaturePolicyHashAlgo);
		// The value of the hash of the signature policy, computed the same way as
		// in clause 5.2.9 of CAdES (ETSI EN 319 122)
		policy.setDigestValue(digestedBytes);


		// We add the policy object to the signature parameters
		signatureParameters.bLevel().setSignaturePolicy(policy);


		// Instantiate a Common Certificate Source object
		CommonCertificateSource ccs = new CommonCertificateSource();
		// Add  the signing certificate to the source obtained in the previous step
		ccs.addCertificate(pk.entry.getCertificate());



		// Instantiate a List of Trusted Lists object
		LOTLSource lotlSource = new LOTLSource();
		// The url where the LOTL needs to be downloaded
		lotlSource.setUrl("https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl-mp.xml");
		// A certificate source which contains the signing certificate(s) for the current list of trusted lists
		lotlSource.setCertificateSource(ccs);
		// true or false for the pivot support. Default = false
		// More information: https://ec.europa.eu/tools/lotl/pivot-lotl-explanation.html
		lotlSource.setPivotSupport(true);


		CommonsDataLoader commonsHttpDataLoader = new CommonsDataLoader();
		FileCacheDataLoader onlineFileLoader = new FileCacheDataLoader(commonsHttpDataLoader);
		CacheCleaner cacheCleaner = new CacheCleaner();
		// Remove the stored file(s) on the file-system
		cacheCleaner.setCleanFileSystem(true);
		// If the file-system cleaner is enabled, inject the configured loader from the
		// online or offline refresh data loader
		cacheCleaner.setDSSFileLoader(onlineFileLoader);


		
		TrustedListsCertificateSource tslCertificateSource = new TrustedListsCertificateSource();
		TLValidationJob validationJob = new TLValidationJob();
		// The TLValidationJob allows to download, parse, validate the Trusted List(s) and Lists Of Trusted Lists (LOTL).
		// Once the task is done, its result is stored in the TrustedListsCertificateSource.
		// The job uses 3 different caches (download, parsing and validation) and a state-machine to be efficient.
		validationJob.setTrustedListCertificateSource(tslCertificateSource);
		validationJob.setOnlineDataLoader(onlineFileLoader);
		validationJob.setCacheCleaner(cacheCleaner);
		// Specify where is the TL/LOTL is hosted and which are the signing certificate(s) for these LOTL
		validationJob.setListOfTrustedListSources(lotlSource);
		// call with the Online Loader
		validationJob.onlineRefresh();

		// configured trusted list certificate source
		commonCertificateVerifier.setTrustedCertSource(tslCertificateSource);

		OnlineCRLSource onlineCRLSource = new OnlineCRLSource();
		// Allows setting an implementation of `DataLoader` interface,
		// processing a querying of a remote revocation server.
		onlineCRLSource.setDataLoader(commonsHttpDataLoader);
		// Configured CRL Access
		commonCertificateVerifier.setCrlSource(onlineCRLSource);

		OCSPDataLoader ocspDataLoader = new OCSPDataLoader();
		OnlineOCSPSource onlineOCSPSource = new OnlineOCSPSource();
		// Allows setting an implementation of `DataLoader` interface,
		// processing a querying of a remote revocation server.
		onlineOCSPSource.setDataLoader(ocspDataLoader);
		// Configured OCSP Access
		commonCertificateVerifier.setOcspSource(onlineOCSPSource);


		// Create ASiCWithXAdES ASiCwithxadesService for signature
		ASiCWithXAdESService service = new ASiCWithXAdESService(commonCertificateVerifier);

        // Set the Timestamp source (Cartão do Cidadão)
		OnlineTSPSource onlineTSPSource = new OnlineTSPSource("http://ts.cartaodecidadao.pt/tsa/server");
		// Allows setting an implementation of `DataLoader` interface,
		// processing a querying of a remote TSP server.
		onlineTSPSource.setDataLoader(new CommonsDataLoader("application/timestamp-query"));
		service.setTspSource(onlineTSPSource);

		
		// Get the SignedInfo segment that need to be signed
		ToBeSigned dataToSign = service.getDataToSign(toPreserve, signatureParameters);
		// This function obtains the signature value for signed information using the private key and specified algorithm
		SignatureValue signatureValue = pk.signatureToken.sign(dataToSign, signatureParameters.getDigestAlgorithm(), pk.entry);
		// We invoke the ASiCwithxadesService to sign the document with the signature value obtained in the previous step
        signedDocument = service.signDocument(toPreserve, signatureParameters, signatureValue);
        
    }
}