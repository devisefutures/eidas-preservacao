/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: ContainerValAndCont
* Class Constructor: ContainerValAndCont(DSSDocument container)
* This class receives as an argument an ASiC container in the form of DSSDocument
*
*
* This class aims to carry out the process of validation of the container as well as having access to its content
* using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/

package com.devisefutures.projeto;

import java.io.IOException;

import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.validation.reports.Reports;

import eu.europa.esig.dss.simplereport.SimpleReport;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.asic.xades.validation.ASiCContainerWithXAdESValidator;
import eu.europa.esig.dss.detailedreport.DetailedReport;

import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerConfigurationException;

import eu.europa.esig.dss.simplereport.SimpleReportFacade;
import eu.europa.esig.dss.simplereport.jaxb.XmlSimpleReport;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import eu.europa.esig.dss.detailedreport.jaxb.XmlDetailedReport;
import eu.europa.esig.dss.detailedreport.DetailedReportFacade;
import eu.europa.esig.dss.diagnostic.DiagnosticData;
import eu.europa.esig.dss.diagnostic.DiagnosticDataFacade;

import eu.europa.esig.validationreport.jaxb.ValidationReportType;
import eu.europa.esig.validationreport.ValidationReportFacade;
import eu.europa.esig.dss.model.InMemoryDocument;

import java.io.ByteArrayInputStream;
import java.io.File;

import eu.europa.esig.dss.service.http.commons.OCSPDataLoader;
import eu.europa.esig.dss.tsl.source.LOTLSource;
import eu.europa.esig.dss.service.http.commons.FileCacheDataLoader;
import eu.europa.esig.dss.tsl.cache.CacheCleaner;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import eu.europa.esig.dss.spi.x509.CommonCertificateSource;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import java.util.List;


import javax.xml.transform.Templates;

import eu.europa.esig.dss.validation.scope.SignatureScope;



public class ContainerValAndCont{
    Reports reports;
    public SimpleReport simpleReport;
    Templates pdfTemplates;
    String bootstrap3Report;
    List<DSSDocument> originalDocuments;
    public List<DSSDocument> retrievedOriginalDocuments;
    List<SignatureScope> signatureScopes ;
    public DSSDocument splReport;
    public DSSDocument detailReport;
    public DSSDocument diagoReport;
    public DSSDocument etsiReport;
    String bootstrap3SimpleReport;
    String bootstrap3DetailedReport;
    String bootstrap3DiagnosticData;
    DetailedReport rp;


    public ContainerValAndCont(DSSDocument container) throws IOException,JAXBException,SAXException,TransformerConfigurationException,XMLStreamException,TransformerException{        
        System.out.println("AQQQQQQQI2"+container.getMimeType());

        // We create an instance of DocumentValidator
        // The method allows instantiation of a related validator for a provided document
        ASiCContainerWithXAdESValidator sdv = new ASiCContainerWithXAdESValidator(container);
        // Sets if the ETSI validation report must be created
        sdv.setEnableEtsiValidationReport(true);


        // Create common certificate verifier
        CommonCertificateVerifier certificateVerifier = new CommonCertificateVerifier();


        // Instantiate a Common Certificate Source object
		CommonCertificateSource cs = new CommonCertificateSource();


	        
        // Instantiate a List of Trusted Lists object
		LOTLSource lotlSource = new LOTLSource();
		// The url where the LOTL needs to be downloaded
		lotlSource.setUrl("https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl-mp.xml");
		// A certificate source which contains the signing certificate(s) for the current list of trusted lists
		lotlSource.setCertificateSource(cs);
		// true or false for the pivot support. Default = false
		// More information: https://ec.europa.eu/tools/lotl/pivot-lotl-explanation.html
		lotlSource.setPivotSupport(true);


		CommonsDataLoader commonsHttpDataLoader = new CommonsDataLoader();
		FileCacheDataLoader onlineFileLoader = new FileCacheDataLoader(commonsHttpDataLoader);
		CacheCleaner cacheCleaner = new CacheCleaner();
		// Remove the stored file(s) on the file-system
		cacheCleaner.setCleanFileSystem(true);
		// If the file-system cleaner is enabled, inject the configured loader from the
		// online or offline refresh data loader
		cacheCleaner.setDSSFileLoader(onlineFileLoader);


		
		TrustedListsCertificateSource tslCertificateSource = new TrustedListsCertificateSource();
		TLValidationJob validationJob = new TLValidationJob();
		// The TLValidationJob allows to download, parse, validate the Trusted List(s) and Lists Of Trusted Lists (LOTL).
		// Once the task is done, its result is stored in the TrustedListsCertificateSource.
		// The job uses 3 different caches (download, parsing and validation) and a state-machine to be efficient.
		validationJob.setTrustedListCertificateSource(tslCertificateSource);
		validationJob.setOnlineDataLoader(onlineFileLoader);
		validationJob.setCacheCleaner(cacheCleaner);
		// Specify where is the TL/LOTL is hosted and which are the signing certificate(s) for these LOTL
		validationJob.setListOfTrustedListSources(lotlSource);
		// call with the Online Loader
		validationJob.onlineRefresh();

		// configured trusted list certificate source
		certificateVerifier.setTrustedCertSource(tslCertificateSource);

		OnlineCRLSource onlineCRLSource = new OnlineCRLSource();
		// Allows setting an implementation of `DataLoader` interface,
		// processing a querying of a remote revocation server.
		onlineCRLSource.setDataLoader(commonsHttpDataLoader);
		// Configured CRL Access
		certificateVerifier.setCrlSource(onlineCRLSource);

		OCSPDataLoader ocspDataLoader = new OCSPDataLoader();
		OnlineOCSPSource onlineOCSPSource = new OnlineOCSPSource();
		// Allows setting an implementation of `DataLoader` interface,
		// processing a querying of a remote revocation server.
		onlineOCSPSource.setDataLoader(ocspDataLoader);
		// Configured OCSP Access
        certificateVerifier.setOcspSource(onlineOCSPSource);
        

        // We add the certificate verifier (which allows to verify and trust certificates)
        sdv.setCertificateVerifier(certificateVerifier);


        // Executes the validation process and produces validation reports:
        // Simple report, Detailed report, Diagnostic data and ETSI Validation Report 
        reports = sdv.validateDocument();


        // The simple report is a summary of the detailed report (more user-friendly)
        simpleReport = reports.getSimpleReport();
        SimpleReportFacade simpleReportFacade = SimpleReportFacade.newFacade();
        // Convert the Simple Report to a XML file
        String marshalledSimpleReport = simpleReportFacade.marshall(reports.getSimpleReportJaxb());
        XmlSimpleReport xmlSimpleReport = simpleReportFacade.unmarshall(marshalledSimpleReport);
        bootstrap3SimpleReport = SimpleReportFacade.newFacade().generateHtmlReport(xmlSimpleReport);
        splReport = new InMemoryDocument(new ByteArrayInputStream(bootstrap3SimpleReport.getBytes()));
        splReport.setName("VAL-C-SimpleReport.xml"); 


        // The detailed report which is the result of the process of the diagnostic data and the validation policy 
        rp = reports.getDetailedReport();
        DetailedReportFacade detailedReportFacade = DetailedReportFacade.newFacade();
        // Convert the Detailed Report to a XML file
        String marshalledDetailedReport = detailedReportFacade.marshall(reports.getDetailedReportJaxb());
        XmlDetailedReport xmlDetailedReport = detailedReportFacade.unmarshall(marshalledDetailedReport);
        bootstrap3DetailedReport = DetailedReportFacade.newFacade().generateHtmlBootstrap3Report(xmlDetailedReport);
        detailReport = new InMemoryDocument(new ByteArrayInputStream(bootstrap3DetailedReport.getBytes())); 
        detailReport.setName("VAL-C-DetailedReport.xml");


        // The diagnostic data which contains all used and static data
        DiagnosticData dd = reports.getDiagnosticData();
        DiagnosticDataFacade diagnosticDataFacade = DiagnosticDataFacade.newFacade();
        // Convert the Diagnostic Data to a XML file
        String marshalledDiagnosticData = diagnosticDataFacade.marshall((reports.getDiagnosticDataJaxb()));
        diagoReport = new InMemoryDocument(new ByteArrayInputStream(marshalledDiagnosticData.getBytes())); 
        diagoReport.setName("VAL-C-DiagnosticData.xml");
      

        // Returns ETSI Validation Report
        ValidationReportFacade facade = ValidationReportFacade.newFacade();
        ValidationReportType etsiValidationReport = reports.getEtsiValidationReportJaxb();
        // Convert the Diagnostic Data to a XML file
        String marshall = facade.marshall(etsiValidationReport, true);
        etsiReport = new InMemoryDocument(new ByteArrayInputStream(marshall.getBytes())); 
        etsiReport.setName("VAL-C-ETSIReport.xml");


        // Builds a List with the content of the container
        retrievedOriginalDocuments = sdv.getOriginalDocuments(simpleReport.getFirstSignatureId());

    }

}