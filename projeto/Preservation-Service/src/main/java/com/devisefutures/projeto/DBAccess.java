/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: DBAccess
* Class Methods: getAipId(String uuid) & deleteAipUuid(String uuid)
* These methods are given as an argument a string representing the unique identifier (uuid) that has associated AIP-IDs
* 
*
* This class aims at the access and/or removal of API-IDs associated with a unique identifier (uuid)
*/

package com.devisefutures.projeto;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import java.util.Map;
import java.util.HashMap;

import com.mongodb.*;
import java.util.List;
import java.util.ArrayList;

public class DBAccess{

    MongoClient mongoClient = new MongoClient("localhost", 27017);
    DB database = mongoClient.getDB("evidences");
    DBCollection collection = database.getCollection("aipIds");



    public String getAipId(String uuid){
        // Get the Map object from the collection
        DBObject docc = collection.findOne();
        // Existing Map object from the collection
        Map <String,BasicDBList> m = docc.toMap();
        //m.remove(m.keySet().toArray()[0]);

        String aipId = "";
        // If the Map Keys contains the uuid
        if(m.containsKey(uuid)){
            // Get the AIP-ID list associated with that uuid
            BasicDBList l = m.get(uuid);
            // Get the last AIP-ID from the list in the previous step
            aipId= (String) l.get(l.size()-1);

        }
        else{
            System.out.println("INVALID UUID");
        }
        //mongoClient.close();

        return aipId;
    }


    public List<String> deleteAipUuid(String uuid){
        // Get the Map object from the collection
        DBObject doc = collection.findOne();
        // Existing Map object from the collection
        Map<String,BasicDBList> b =  (HashMap <String,BasicDBList>) doc.toMap();
        //b.remove(b.keySet().toArray()[0]);
        List<String> res = new ArrayList<String>();

        for(Object el: b.get(uuid)) {
            // Buid a list with all AIP-ID associated with that uuid to later removal from archival service
            res.add((String) el);
        }
        // Remove that entry from the database
        b.remove(uuid);
        doc = collection.findOne();
        //System.out.println(res);
        // Update the database
        WriteResult on = collection.update(doc ,new BasicDBObject(b));
        //mongoClient.close();

        return res;

    }  
}