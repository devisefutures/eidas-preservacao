/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: DBAInsert
* Class Methods: dbInsert(String aipId) & aipUpdate(String uuid,String newAip)
* The first method receives as argument a string representing an AIP-ID that does not yet have a 
* unique identifier (uuid) associated
* The second method receives as argument a string representing a unique identifier (uuid) 
* and another string representing an AIP ID to make a new association
* 
*
* This class aims to give a new entry in the database of an AIP-ID generating a unique identifier (uuid),
* as well as to make an update in one of the database entries
*/

package com.devisefutures.projeto;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import java.util.Map;
import java.util.HashMap;


import com.mongodb.*;

import java.io.IOException;

import java.util.UUID;



public class DBInsert{

    MongoClient mongoClient = new MongoClient("localhost", 27017);
    DB database = mongoClient.getDB("evidences");
    DBCollection c = database.createCollection("aipIds", null);
    DBCollection collection ;

    public String dbInsert(String aipId) throws IOException{     
        // Mongo collection
        collection = database.getCollection("aipIds");
        // Get the Map object from the collection
        DBObject doc = collection.findOne();
        // Build a list with AIP-IDs
        BasicDBList aips = new BasicDBList();
        aips.add(aipId);

        UUID uuid;

        // if the Map object is null
        if(doc == null){
            // Create new Map
            Map <String,BasicDBList> ola = new HashMap<String,BasicDBList>();
            // Generate new uuid
            uuid = UUID.randomUUID();
            // Associate the list of AIP-IDs with the uuid
            ola.put(uuid.toString(), aips);
            // Insertion in the database
            collection.insert(new BasicDBObject(ola));
        }
        else{
            // Existing Map object from the collection
            Map<String,BasicDBList> b =  (HashMap <String,BasicDBList>) doc.toMap();
            //b.remove(b.keySet().toArray()[0]);
            // Generate new uuid
            uuid = UUID.randomUUID();
            // Making sure that there is not yet that uuid
            while(b.containsKey(uuid.toString())){
                uuid = UUID.randomUUID();
            }
            // Add the new Map Entry
            b.put(uuid.toString(), aips);
    
            // Update the database
            WriteResult on= collection.update(doc,new BasicDBObject(b));
        }
        //mongoClient.close();

        return uuid.toString();
    }

    public void aipUpdate(String uuid,String newAip){
        // Mongo collection
        collection = database.getCollection("aipIds");
        // Get the Map object from the collection
        DBObject doc = collection.findOne();

        // Existing Map object from the collection
        Map<String,BasicDBList> b =  (HashMap <String,BasicDBList>) doc.toMap();
        //b.remove(b.keySet().toArray()[0]);
        
        // Get the AIP-ID list associated with that uuid
        BasicDBList l = b.get(uuid);

        // Add the new AIP-ID
        l.add(newAip);
        // Add the new Map Entry
        b.put(uuid, l);
        doc = collection.findOne();

        // Update the database
        WriteResult on = collection.update(doc ,new BasicDBObject(b));
        //mongoClient.close();

    }
}