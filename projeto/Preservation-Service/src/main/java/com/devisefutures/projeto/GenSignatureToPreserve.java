/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: GenSignatureToPreserve
*
* This class aims to create advanced signatures -LTA to test the Proof of Concept system using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/

package com.devisefutures.projeto;

import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import eu.europa.esig.dss.enumerations.SignatureLevel;

public class GenSignatureToPreserve {
    
        public GenSignatureToPreserve() throws IOException, URISyntaxException {
        // Instantiate the Mock User KeyStore object
        Pkc12 pk = new Pkc12(1);

        File dir = new File(System.getProperty("user.home")+"/Desktop/"+"toPreserve");

        if(dir.mkdir()){
            // Documents to sign
            DSSDocument documentToSignXadES = new InMemoryDocument(getClass().getResourceAsStream("/toSign/sample.xml"));
            DSSDocument documentToSignPadES = new InMemoryDocument(getClass().getResourceAsStream("/toSign/teste.pdf"));
            DSSDocument documentToSignCadES = new InMemoryDocument(getClass().getResourceAsStream("/toSign/sample.xml"));

            // Creation of a XadES-LTA signature
            XadES x = new XadES(documentToSignXadES,SignatureLevel.XAdES_BASELINE_LTA,pk);
            // Path to save the signature file
            x.signedDocument.save(System.getProperty("user.home")+"/Desktop/toPreserve/"  + x.signedDocument.getName());

            // Creation of a PadES-LTA signature
            PadES p = new PadES(documentToSignPadES,SignatureLevel.PAdES_BASELINE_LTA,pk);
            // Path to save the signature file
            p.signedDocument.save(System.getProperty("user.home")+"/Desktop/toPreserve/" + p.signedDocument.getName());


            // Creation of a CadES-LTA signature
            CadES c = new CadES(documentToSignCadES,SignatureLevel.CAdES_BASELINE_LTA,pk);
            // Path to save the signature file
            c.signedDocument.save(System.getProperty("user.home")+"/Desktop/toPreserve/" + c.signedDocument.getName());
        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        GenSignatureToPreserve g = new GenSignatureToPreserve();
    }
    
}