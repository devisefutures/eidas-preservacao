/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: LoggerPs
* Class Methods: logMessage(String txt)
* This method receives as argument a string representing the text to register in the log file
*
*
* This class aims at building and updating a Log File
*/

package com.devisefutures.projeto;

import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.io.IOException;

public class LoggerPS {
    public void logMessage(String txt) {  

        Logger logger = Logger.getLogger("PreservationServiceLog");  
        FileHandler fh;  
        logger.setUseParentHandlers(false);
        try {  
    
            // This block configure the logger with handler and formatter  
            fh = new FileHandler(System.getProperty("user.home")+"/Desktop"+"/PreservationServiceLog.log",true);  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  
    
            // the following statement is used to log any messages  
            logger.info(txt);  
            fh.close();
    
        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    
    }

    public static void main(String[] args) {
        LoggerPS l = new LoggerPS();
        l.logMessage("txt");
    }
}