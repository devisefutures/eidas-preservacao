/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: Pkc12
* Class Constructor: Pkc12(int s)
* This class receives as an argument an int representing the keystore to instantiate
*
* To use DDS Java code, we need to specify the type of KeyStore to use for signing our document and create the preservation evidence, more simply, where the private key can be found. 
* In the package "eu.europa.esig.dss.token", we can choose between different connection tokens.
* Pkcs12SignatureToken was chosen : allows signing with a PKC#12 keystore (.p12 file).
* This standard defines a file format commonly used to store the private key and corresponding public key certificate protecting them by password.
*
*
* For this Proof of Concept two Pck12 keystores are used and managed using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
* One for the creation of signatures to test the Proof of Concept system (s=1). 
* Other for creating the preservation evidence (s=0)
*/

package com.devisefutures.projeto;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore.PasswordProtection;
import java.util.List;

import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;





public class Pkc12 {

    KSPrivateKeyEntry dssPrivateKeyEntry;
    DSSPrivateKeyEntry entry;
    Pkcs12SignatureToken signatureToken;
    
    public Pkc12(int s) throws IOException {
        if(s == 0){
            try (
            InputStream keystorepath = getClass().getResourceAsStream("/keystore/PKCS12_Credential_presservproofofconcept@gmail.com.pfx");
            // Creates a SignatureTokenConnection with the provided filepath to PKCS#12 KeyStore file and password.
            Pkcs12SignatureToken signatureTokenAux = new Pkcs12SignatureToken(keystorepath, new PasswordProtection("1yjK8TTa9KbW".toCharArray()))) {
                    // Retrieves all the available keys (private keys entries) from the token.
                    List<DSSPrivateKeyEntry> keys = signatureTokenAux.getKeys();
                    signatureToken = signatureTokenAux;
                    // Wrapper of a PrivateKeyEntry coming from a KeyStore.
                    dssPrivateKeyEntry = (KSPrivateKeyEntry) keys.get(0);
                    // Interface for a PrivateKey
                    entry = signatureTokenAux.getKey(dssPrivateKeyEntry.getAlias(), new PasswordProtection("1yjK8TTa9KbW".toCharArray()));
            }
        }
        else if(s == 1){
            try (
            InputStream keystorepath = getClass().getResourceAsStream("/keystore/PKCS12_Credential_mock.user.sign@gmail.com.pfx");
            Pkcs12SignatureToken signatureTokenAux = new Pkcs12SignatureToken(keystorepath, new PasswordProtection("53Ru9gBkuY6G".toCharArray()))) {
                    // Retrieves all the available keys (private keys entries) from the token.
                    List<DSSPrivateKeyEntry> keys = signatureTokenAux.getKeys();
                    signatureToken = signatureTokenAux;
                    // Wrapper of a PrivateKeyEntry coming from a KeyStore.
                    dssPrivateKeyEntry = (KSPrivateKeyEntry) keys.get(0);
                    // Interface for a PrivateKey
                    entry = signatureTokenAux.getKey(dssPrivateKeyEntry.getAlias(), new PasswordProtection("53Ru9gBkuY6G".toCharArray()));
		    }
        }
    }
}

