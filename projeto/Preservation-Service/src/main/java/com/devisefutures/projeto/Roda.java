/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: Roda
* Class Methods: transferRoda(DSSDocument evidence) & getFileFromAip(String aipId) & deleteAIP(String aipId)
* The first method receives as argument an ASiC Container in the form of DSSDocument (preservation evidence) to send to the archival service
* The second method receives as argument a string representing an AIP-ID of preservation evidence to be returned 
* The third method receives as argument a string representing an AIP-ID of preservation evidence to be deleted 
*
*
* The purpose of this class is to communicate with the archival service.It is responsible for sending preservation evidence,
* requesting preservation evidence and lastly erasing preservation evidence from the archival service
*/

package com.devisefutures.projeto;


import java.net.ProtocolException;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;

import java.io.File;



import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpDelete;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;


import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.http.util.EntityUtils;

import java.lang.String;
import org.json.*;
import org.apache.http.entity.StringEntity;
import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import java.lang.Thread;
import java.lang.InterruptedException;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.util.*;


public class Roda{
    // Username and password credentials
    byte[] credentials = Base64.encodeBase64(("admin" + ":" + "roda").getBytes(StandardCharsets.UTF_8));
    // Base Url
    String baseUrl = "http://localhost:8080/api/v1/";


    // Tranfer to archival service
    public String transferRoda(DSSDocument evidence) throws ProtocolException,IOException,InterruptedException{
        // Prepare the evidence document to ingest
        InputStream in = evidence.openStream();
        byte[] buffer = new byte[in.available()];
        in.read(buffer);
        File file = new File(System.getProperty("user.home")+"/Downloads/" + evidence.getName());
        OutputStream outStream = new FileOutputStream(file);
        outStream.write(buffer);
        outStream.close();


        // POST request
        HttpPost post = new HttpPost( this.baseUrl + "transfers");
        post.setHeader("Authorization", "Basic " + new String(this.credentials, StandardCharsets.UTF_8));

        // Prepare the file body
        FileBody fileBody = new FileBody(file);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("upl", fileBody);


        HttpEntity entity = builder.build();
        post.setEntity(entity);

     
        // Response
        HttpResponse response = HttpClientBuilder.create().build().execute(post);
        String jsonString = EntityUtils.toString(response.getEntity());
        String aip_id="";
        file.delete();

      
        // Parse the response to get the AIP-ID
        if(response.getStatusLine().getStatusCode() == 200){
            JSONObject obj = new JSONObject(jsonString);
            String transf_uuid = obj.getString("uuid");
            System.out.println("TRANSFER COMPLETED WITH ID= " + transf_uuid);
            // AIP Creation
            this.createAIP(transf_uuid);
            Thread.sleep(3000);
            // Get AIP-ID from previous step
            aip_id = this.getAIPIdFromTransfer(transf_uuid);

        }
        else{
            System.out.println(response.getStatusLine() + " erro na transferencia");
            
        }
        return aip_id;

    }

    // AIP Creation (this method is used by the trasferRoda() method)
    public void createAIP(String tranf_uuid) throws UnsupportedEncodingException,ProtocolException,IOException{
        HttpPost post = new HttpPost(this.baseUrl + "jobs");

        post.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        StringEntity params = new StringEntity("{ \"id\": \"string\", \"name\": \"cont ingest\", \"username\": \"admin\", \"plugin\": \"org.roda.core.plugins.plugins.ingest.ConfigurableIngestPlugin\", \"pluginType\": \"INGEST\", \"pluginParameters\": { \"parameter.force_parent_id\": \"false\", \"parameter.do_virus_check\": \"true\", \"parameter.notification_when_failed\": \"false\", \"parameter.sip_to_aip_class\": \"org.roda.core.plugins.plugins.ingest.TransferredResourceToAIPPlugin\", \"parameter.create.premis.skeleton\": \"true\", \"parameter.do_producer_authorization_check\": \"true\", \"parameter.reporting_class\": \"org.roda.core.plugins.plugins.ingest.ConfigurableIngestPlugin\", \"parameter.do_auto_accept\": \"true\", \"parameter.create_submission\": \"false\", \"parameter.do_descriptive_metadata_validation\": \"true\", \"parameter.do_file_format_identification\": \"true\" }, \"sourceObjects\": { \"type\": \"list\", \"ids\": [ \"" + tranf_uuid + "\" ], \"selectedClass\": \"org.roda.core.data.v2.ip.TransferredResource\" },\t \"inFinalState\": true, \"stopping\": true} ");
        post.addHeader("content-type", "application/json");
        post.setEntity(params);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        if(response.getStatusLine().getStatusCode() == 201){
            System.out.println("AIP CREATED");
        }

    }

    // Get AIP-ID (this method is used by the trasferRoda() method)
    public String getAIPIdFromTransfer(String tranf_uuid) throws UnsupportedEncodingException,ProtocolException,IOException{
        HttpGet get = new HttpGet(this.baseUrl + "reports/last?id=" + tranf_uuid + "&acceptFormat=json");

        get.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        String jsonString = EntityUtils.toString(response.getEntity());
        String aip_id ="";
        if(response.getStatusLine().getStatusCode() == 200){
            JSONObject obj = new JSONObject(jsonString);
            aip_id = obj.getString("outcomeObjectId"); 
            System.out.println("AIP ID= " + aip_id);

        }
        else{
            System.out.println(response.getStatusLine() + " no getAIP");
        }
        return aip_id;

    }

    // This method isn't used
    public String getRepresentationId(String aipId) throws UnsupportedEncodingException,ProtocolException,IOException{
        HttpGet get = new HttpGet(this.baseUrl + "aips/" + aipId + "?acceptFormat=json");

        get.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        HttpResponse response = HttpClientBuilder.create().build().execute(get);
        String jsonString = EntityUtils.toString(response.getEntity());
        String representation_id = "";
        if(response.getStatusLine().getStatusCode() == 200){
            JSONObject obj = new JSONObject(jsonString);

            JSONArray arr = obj.getJSONArray("representations");
            representation_id = arr.getJSONObject(0).getString("id");
            System.out.println("REPRESENTATION ID = " + representation_id);
        }else{
            System.out.println(response.getStatusLine());

        }
        return representation_id;

    }

    // Get Evidence File from the archival service
    public DSSDocument getFileFromAip(String aipId) throws UnsupportedEncodingException,ProtocolException,IOException{
        // Obtain the identifier of the file present in the AIP
        Map<String, String> res = getFileIdFromAip(aipId);

        // Parsing the file information
        String file_name = res.get("file_name");
        String file_uuid = res.get("file_uuid");

        // GET Request
        HttpGet get = new HttpGet(this.baseUrl + "files/" + file_uuid + "?acceptFormat=bin");
        get.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        // Response
        HttpResponse response = HttpClientBuilder.create().build().execute(get);
        DSSDocument doc = null;

        if(response.getStatusLine().getStatusCode() == 200){
            // Preservation Evidence (ASiC container) from arquival service
            doc = new InMemoryDocument(response.getEntity().getContent(),file_name); 

        }else{
            System.out.println(response.getStatusLine()+ " erro no getFile");

        }
        return doc;

    }

    // Get identifier of the file present in the AIP (this method is used by the getFileFromAip() method)
    public  Map<String, String> getFileIdFromAip(String aipId) throws UnsupportedEncodingException,ProtocolException,IOException{
        HttpGet get = new HttpGet(this.baseUrl + "index?returnClass=org.roda.core.data.v2.ip.IndexedFile&filter=aipId=" + aipId + "&sort=uuid%20desc");

        get.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        HttpResponse response = HttpClientBuilder.create().build().execute(get);
        String jsonString = EntityUtils.toString(response.getEntity());
        Map<String, String> res = new HashMap<String, String>();
       

        if(response.getStatusLine().getStatusCode() == 200){
            JSONObject obj = new JSONObject(jsonString);
            //System.out.println(obj);

            JSONArray arr = obj.getJSONArray("results");

            String file_uuid = arr.getJSONObject(0).getJSONObject("file").getString("uuid");

            String file_name =  arr.getJSONObject(0).getJSONObject("file").getString("id");

            res.put("file_name",file_name);
            res.put("file_uuid",file_uuid);

            System.out.println("FILE= "+ file_name+ " UUID= " + file_uuid);

        }else{
            System.out.println(response.getStatusLine() + " erro no getFileId");

        }
        return res;

    }

 
    // Delete AIP (it is necessary to delete the AIP created as well as the file of the transfers from the archive service)
    public boolean deleteAIP(String aipId) throws UnsupportedEncodingException,ProtocolException,IOException{
        boolean i = false;
        HttpGet get = new HttpGet(this.baseUrl + "aips/" + aipId + "?acceptFormat=json");

        get.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

        HttpResponse response0 = HttpClientBuilder.create().build().execute(get);
        String jsonString = EntityUtils.toString(response0.getEntity());
        String transf_id  = "";

        if(response0.getStatusLine().getStatusCode() == 200){
            JSONObject obj = new JSONObject(jsonString);

            transf_id = obj.getString("ingestSIPUUID");
            i=true;

        }else{
            System.out.println(response0.getStatusLine() + " erro busca do idTransfer");
            i=false;        
        }

        // AIP DELETE Request 
        HttpDelete deleteAIP = new HttpDelete(this.baseUrl + "aips/" + aipId);
        deleteAIP.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));
        deleteAIP.setHeader("Content-type", "application/x-www-form-urlencoded");
        // AIP DELETE Response 
        HttpResponse response = HttpClientBuilder.create().build().execute(deleteAIP);

        if(response.getStatusLine().getStatusCode() == 200){
            System.out.println("AIP DELETED");
            i=true;

        }else{
            System.out.println(response.getStatusLine() + " erro deleteAIP");
            i=false;
        }

        // file DELETE Request 
        HttpDelete deleteFile = new HttpDelete(this.baseUrl + "transfers/?transferred_resource_ids=" + transf_id);
        deleteFile.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));
        // file DELETE Response
        HttpResponse response1 = HttpClientBuilder.create().build().execute(deleteFile);

        if(response1.getStatusLine().getStatusCode() == 200){
            System.out.println("FILE DELETED");
            i=true;
        
        }else{
            System.out.println(response1.getStatusLine() + "erro deleteFile");
            i=false;
        }

        return i;
    }
}