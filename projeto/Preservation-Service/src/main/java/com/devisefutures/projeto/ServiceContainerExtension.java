/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: ServiceContainerExtension
* Class Methods: extendContainers()
*
*
* This class aims to extend all the advanced XAdES-LTA signature associated with the container (preservation evidence) 
* presents in the archival system using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/

package com.devisefutures.projeto;

import eu.europa.esig.dss.model.DSSDocument;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;
import java.io.IOException;
import javax.xml.transform.TransformerConfigurationException;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.DBCollection;
import com.mongodb.*;
import java.util.*;

public class ServiceContainerExtension{
    ASiCsXAdESExtension extendedContainer;

    public void extendContainers() throws InterruptedException,IOException,JAXBException,SAXException,TransformerConfigurationException,XMLStreamException,TransformerException{
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB database = mongoClient.getDB("evidences");
        // Mongo collection
        DBCollection collection = database.getCollection("aipIds");
        // Get the Map object from the collection
        DBObject docc = collection.findOne();
        // Existing Map object from the collection
        Map <String,BasicDBList> m = docc.toMap();
        //m.remove(m.keySet().toArray()[0]);

        // Prepare a list with all uuids (keys from the Map) 
        List<String> li = new ArrayList<String>(m.keySet());
        // Instantiate the RODA archival service
        Roda r = new Roda();
        // Instantiate the DBInsert service
        DBInsert db = new DBInsert();
        // Instantiate the LoggerPs service
        LoggerPS log = new LoggerPS();

        // For each uuid
        for(String uuid : li){
            // Get all AIP-IDs associated
            BasicDBList l = m.get(uuid);
            // Get the last AIP-ID
            String aipId= (String) l.get(l.size()-1);
            // Get the container from the archival service with that AIP-ID
            DSSDocument cont = r.getFileFromAip(aipId);

            // Instantiate the Container Validation service
            ContainerValAndCont cv = new ContainerValAndCont(cont);

            // If the report status == TOTAL-PASSED
            if(cv.simpleReport.isValid(cv.simpleReport.getFirstSignatureId())){
                // Instantiate the ASiC with XadES Extension
                extendedContainer = new ASiCsXAdESExtension(cont);
                extendedContainer.extendedContainer.setName("Extended_"+cont.getName());
                // Send the extended container to the archival service
                String aip_id = r.transferRoda(extendedContainer.extendedContainer);
                // Update the database with the AIP-ID from the extended container
                db.aipUpdate(uuid, aip_id);
                // Log the action
                log.logMessage("Preservation Evidence with UUID: " + uuid +"  Exetended.");
                mongoClient.close();
            }else{
                System.out.println("Cont invalido");
            }
            
        }

    }

    public static void main(String[] args) throws InterruptedException,IOException,JAXBException,SAXException,TransformerConfigurationException,XMLStreamException,TransformerException{
   
        ServiceContainerExtension ex =  new ServiceContainerExtension();
        ex.extendContainers();
    }
}