/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: ServiceInit
* Class Methods: initServiceWithStorage(DSSDocument signature) & serviceInitWithoutStorage(DSSDocument signature)
* These methods receive as an argument the signature document in the form of a DSSDocument. 
* The first method is responsible for the preservation profile with storage and 
* the second method is responsible for the preservation profile without storage.
*
*
* This class aims to begin the process of preservation of the Proof of Concept using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/

package com.devisefutures.projeto;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;
import java.io.IOException;

import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.MimeType;

import java.util.Map;
import java.util.Date;

import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.io.UnsupportedEncodingException;

public class ServiceInit {

    public Date max = null;

    public String initServiceWithStorage(DSSDocument signature)
            throws IOException, JAXBException, SAXException, XMLStreamException, UnsupportedEncodingException,
            ProtocolException, IOException, InterruptedException, TransformerException, URISyntaxException {
        // Instantiate the Preservation Service Pkc12 keystore object
        Pkc12 pk = new Pkc12(0);

        // Instantiate the Signature Validation Service
        SigValidation sv = new SigValidation(signature);
       

        String aip_id="";
        String uuid="";

        // Get the Simple Report (Validation Report)
        DSSDocument simpleReport = sv.splReport;
        // Get the Detailed Report (Validation Report)
        DSSDocument detailedReport = sv.detailReport;
        // Get the Diagnostic Data (Validation Report)
        DSSDocument diagnosticData = sv.diagoReport;
        // Get the Etsi Report (Validation Report)
        DSSDocument etsiReport = sv.etsiReport; 

        // Build a list that will be the content of the ASiC container (preservation evidence)
        List <DSSDocument> toPreserve = new ArrayList<>();
        // Add the original submission
        toPreserve.add(signature);

        // Check the signature format (if XadES, CadES or PadES)
        if(signature.getMimeType().equals(MimeType.XML)){
            // Creation of a new signature involving the original submission
            XadES x = new XadES(signature, SignatureLevel.XAdES_BASELINE_LTA,pk);
            // Add the new signature to the list to preserve
            toPreserve.add(x.signedDocument);
        }
        else if(signature.getMimeType().equals(MimeType.PDF)){
            // Creation of a new signature involving the original submission
            PadES p = new PadES(signature, SignatureLevel.PAdES_BASELINE_LTA,pk);
            // Add the new signature to the list to preserve
            toPreserve.add(p.signedDocument);
        }
        else{
            // Creation of a new signature involving the original submission
            CadES c = new CadES(signature, SignatureLevel.CAdES_BASELINE_LTA,pk);
            // Add the new signature to the list to preserve
            toPreserve.add(c.signedDocument);
        }

        // Add the Validation Reports to the list to preserve
        toPreserve.add(simpleReport);
        toPreserve.add(detailedReport);
        toPreserve.add(diagnosticData);
        toPreserve.add(etsiReport);
          
        // Instantiate the ASiCSXadESSignLTA service
        ASiCSXAdESSignLTALevel xadesLtaContainer = new ASiCSXAdESSignLTALevel(toPreserve);
        // ASiCS with XadES-LTA (preservation evidence) 
        DSSDocument evidence = xadesLtaContainer.signedDocument;
        // Set the document name
        evidence.setName(signature.getName() + evidence.getName());


        // Instantiate the Roda archival service
        Roda job = new Roda();
        // Send the preservation evidence to the archival service
        aip_id = job.transferRoda(evidence);
        // Instantiate the DBInsert service
        DBInsert db = new DBInsert();
        // Make a new entry in the database
        uuid = db.dbInsert(aip_id);

        // unique identifier associated with AIP-IDs
        return uuid;
    }

    public Map<String,DSSDocument> serviceInitWithoutStorage(DSSDocument signature) throws IOException,JAXBException,SAXException,XMLStreamException ,UnsupportedEncodingException,ProtocolException,IOException,InterruptedException,TransformerException,
            URISyntaxException {

        // Instantiate the Preservation Service Pkc12 keystore object
        Pkc12 pk = new Pkc12(0);

        // Instantiate the Map object to return to the client
        Map <String,DSSDocument> res = new HashMap<String,DSSDocument>();
        // Build a list that will be the content of the ASiC container (preservation evidence)
        List <DSSDocument> toPreserve = new ArrayList<>();

        // Add the original submission to the return Map
        res.put("0", signature);

        // Instantiate the Signature Validation Service
        SigValidation sv = new SigValidation(signature);

        // Check the signature format (if XadES, CadES or PadES)
        if(signature.getMimeType().equals(MimeType.XML)){
            // Creation of a new signature involving the original submission
            XadES x = new XadES(signature, SignatureLevel.XAdES_BASELINE_LTA,pk);
            // Add the new signature to the return Map
            res.put("1", x.signedDocument);
            // Add the new signature to the list to preserve
            toPreserve.add(x.signedDocument);
        }
        else if(signature.getMimeType().equals(MimeType.PDF)){
            // Creation of a new signature involving the original submission
            PadES p = new PadES(signature, SignatureLevel.PAdES_BASELINE_LTA,pk);
            res.put("1", p.signedDocument);
            // Add the new signature to the return Map
            toPreserve.add(p.signedDocument);
            // Add the new signature to the list to preserve
        }
        else{
            // Creation of a new signature involving the original submission
            CadES c = new CadES(signature, SignatureLevel.CAdES_BASELINE_LTA,pk);
            res.put("1", c.signedDocument);
            // Add the new signature to the return Map
            toPreserve.add(c.signedDocument);
            // Add the new signature to the list to preserve
        }

        // Get the Simple Report (Validation Report)
        DSSDocument simpleReport = sv.splReport;
        // Get the Detailed Report (Validation Report)
        DSSDocument detailedReport = sv.detailReport;
        // Get the Diagnostic Data (Validation Report)
        DSSDocument diagnosticData = sv.diagoReport;
        // Get the Etsi Report (Validation Report)
        DSSDocument etsiReport = sv.etsiReport; 
        // Add the Validation Reports to the return Map
        res.put("2", simpleReport);
        res.put("3", detailedReport);
        res.put("4", diagnosticData);
        res.put("5", etsiReport);


        // Add the original submission to the list to preserve
        toPreserve.add(signature);
        // Add the Validation Reports to the list to preserve
        toPreserve.add(simpleReport);
        toPreserve.add(detailedReport);
        toPreserve.add(diagnosticData);
        toPreserve.add(etsiReport);

          
 
        // Instantiate the ASiCSXadESSignLTA service
        ASiCSXAdESSignLTALevel xadesLtaContainer = new ASiCSXAdESSignLTALevel(toPreserve);
        // ASiCS with XadES-LTA (preservation evidence) 
        DSSDocument evidence = xadesLtaContainer.signedDocument;
        // Set the document name
        evidence.setName(signature.getName() + evidence.getName());
        // Add the container to the return Map
        res.put("6", evidence);

        //evidence.save(System.getProperty("user.home")+"/Downloads/" + evidence.getName());

        // Instantiate the Container Validation service
        ContainerValAndCont val =  new ContainerValAndCont(evidence);
        // Expected Evidence Duration
        max = val.simpleReport.getSignatureExtensionPeriodMax(val.simpleReport.getFirstSignatureId());

        // Add the Container Validation Reports to the return Map
        res.put("7",val.splReport);
        res.put("8",val.detailReport);
        res.put("9",val.diagoReport);
        res.put("10",val.etsiReport);

        return res;
    }
}