/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: ServiceUpdateOriginalSub
* Class Methods: updateOriginal(String uuid, DSSDocument signature)
* This method receives as argument a string representing the user's unique identifier 
* and the new signature file in the form of DSSDocument
*
*
* This class aims to perform the action of updating the original submission, which is exclusive to users 
* who have chosen the preservation profile with storage using the DSS - Digital Signature Service project
* (https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html#_introduction)
*/
package com.devisefutures.projeto;

import java.util.List;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;
import java.io.IOException;

import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.MimeType;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.io.UnsupportedEncodingException;

public class ServiceUpdateOriginalSub {

    public boolean updateOriginal(String uuid, DSSDocument signature)
            throws IOException, JAXBException, SAXException, XMLStreamException, UnsupportedEncodingException,
            ProtocolException, IOException, InterruptedException, TransformerException, URISyntaxException {
        // Instantiate the Preservation Service Pkc12 keystore object
        Pkc12 pk = new Pkc12(0);
        boolean res = false;
        // Instantiate the DBAccess service
        DBAccess db0 = new DBAccess();
        // Get the AIP-ID associated with the uuid received
        String aipId = db0.getAipId(uuid);

        // If there is an AIP-ID associated with uuid
        if(aipId!=""){
            // Instantiate the Signature Validation Service
            SigValidation sv = new SigValidation(signature);
            String aip_id="";

            // Get the Simple Report (Validation Report)
            DSSDocument simpleReport = sv.splReport;
            // Get the Detailed Report (Validation Report)
            DSSDocument detailedReport = sv.detailReport;
            // Get the Diagnostic Data (Validation Report)
            DSSDocument diagnosticData = sv.diagoReport;
            // Get the Etsi Report (Validation Report)
            DSSDocument etsiReport = sv.etsiReport; 

            // Build a list that will be the content of the ASiC container (preservation evidence)
            List <DSSDocument> toPreserve = new ArrayList<>();
            // Add the new submission
            toPreserve.add(signature);


            // Check the signature format (if XadES, CadES or PadES)
            if(signature.getMimeType().equals(MimeType.XML)){
                // Creation of a new signature involving the original submission
                XadES x = new XadES(signature, SignatureLevel.XAdES_BASELINE_LTA,pk);
                // Add the new signature to the list to preserve
                toPreserve.add(x.signedDocument);
            }
            else if(signature.getMimeType().equals(MimeType.PDF)){
                // Creation of a new signature involving the original submission
                PadES p = new PadES(signature, SignatureLevel.PAdES_BASELINE_LTA,pk);
                // Add the new signature to the list to preserve
                toPreserve.add(p.signedDocument);
            }
            else{
                // Creation of a new signature involving the original submission
                CadES c = new CadES(signature, SignatureLevel.CAdES_BASELINE_LTA,pk);
                // Add the new signature to the list to preserve
                toPreserve.add(c.signedDocument);
            }

            // Add the Validation Reports to the list to preserve
            toPreserve.add(simpleReport);
            toPreserve.add(detailedReport);
            toPreserve.add(diagnosticData);
            toPreserve.add(etsiReport);
   
    
            // Instantiate the ASiCSXadESSignLTA service
            ASiCSXAdESSignLTALevel xadesLtaContainer = new ASiCSXAdESSignLTALevel(toPreserve);
            // ASiCS with XadES-LTA (preservation evidence) 
            DSSDocument evidence = xadesLtaContainer.signedDocument;
            // Set the document name
            evidence.setName(signature.getName() + evidence.getName());


            // Instantiate the Roda archival service
            Roda job = new Roda();
            // Send the new preservation evidence to the archival service
            aip_id = job.transferRoda(evidence);
            // Instantiate the DBInsert service
            DBInsert db = new DBInsert();
            // Update the database with the new association
            db.aipUpdate(uuid, aip_id);

            res=true;
        }
        else{
            res=false;
            System.out.println("Object not found");
        }
        return res;
    }
}