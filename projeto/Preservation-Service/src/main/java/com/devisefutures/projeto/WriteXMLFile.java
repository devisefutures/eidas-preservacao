/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: WriteXMLFile
* Class Method: createProfileFile(String strMod ,String gol, Date max)
* This method receives as argument a string that represents the chosen storage model, another string that represents the 
* preservation objective and lastly, a string that represents the expected duration of the preservation evidence
*
* This class aims to create an XML file representing the chosen preservation profile
*/

package com.devisefutures.projeto;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.model.DSSDocument;
import java.io.IOException;
import java.util.Date;
import java.io.ByteArrayOutputStream;

public class WriteXMLFile {

	public DSSDocument createProfileFile(String strMod ,String gol, Date max) throws IOException{
        DSSDocument pol =null;
	  try {

          DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
          DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

          // root elements PreservationService
          Document doc = docBuilder.newDocument();
          Element rootElement = doc.createElement("PreservationSrevice");
          doc.appendChild(rootElement);

          // Preservation Profile Element
          Element profile = doc.createElement("PreservationProfile");
          rootElement.appendChild(profile);

          // Storage Model Element
          Element storage = doc.createElement("storageModel");
          storage.appendChild(doc.createTextNode(strMod));
          profile.appendChild(storage);

          // Preservation Goal Element
          Element goal = doc.createElement("preservationGoal");
          goal.appendChild(doc.createTextNode(gol));
          profile.appendChild(goal);

          // Content Element
          Element policy = doc.createElement("content");
          policy.setAttribute("id", "Preservation Evidence Container Content");
          profile.appendChild(policy);
          
          Element point = doc.createElement("point");
          point.appendChild(doc.createTextNode("Original Submission"));
          policy.appendChild(point);
          Element pointt = doc.createElement("point");
          pointt.appendChild(doc.createTextNode("Original Submissition Signed with AdES LTA"));
          policy.appendChild(pointt);
          Element pointtt = doc.createElement("point");
          pointtt.appendChild(doc.createTextNode("Original Submission Validation Reports DSS"));
          policy.appendChild(pointtt);


          // Expected Duration Element
          Element pres_period = doc.createElement("Duration");
          pres_period.setAttribute("id", "Expected Evidence Duration (without storage) and Preservation Period (with storage)");
          Element maxi = doc.createElement("max");
          maxi.appendChild(doc.createTextNode("extend your evidence before: "+max));
          pres_period.appendChild(maxi);

          profile.appendChild(pres_period);

          // Policy Element
          Element policyy = doc.createElement("policy");
          policyy.setAttribute("id", "Preservation Service Policy available at:");
          Element link = doc.createElement("link");
          link.appendChild(doc.createTextNode("https://www.devisefutures.com/pdf/preservacao/PoliticaPreservacao.pdf"));
          policyy.appendChild(link);

          profile.appendChild(policyy);


          // write the content into xml file
          TransformerFactory transformerFactory = TransformerFactory.newInstance();
          Transformer transformer = transformerFactory.newTransformer();
          DOMSource source = new DOMSource(doc);
          
          // transform the file in a DSSDocument
          ByteArrayOutputStream bos=new ByteArrayOutputStream();
          StreamResult result=new StreamResult(bos);
          transformer.transform(source, result);
          byte []array=bos.toByteArray();

          pol = new InMemoryDocument(array);
          pol.setName("preservationProfile.xml");




        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
          }
          return pol;
  }

}

