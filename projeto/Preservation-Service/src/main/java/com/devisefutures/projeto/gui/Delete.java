/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: Delete
*
* This class is intended for the user to perform the delete operation by collecting the user's unique identifier
*/

package com.devisefutures.projeto.gui;

import com.devisefutures.projeto.*;
import javax.swing.JOptionPane;
import java.net.ProtocolException;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;


public class Delete extends javax.swing.JFrame {
    // unique identifier of the user
    String uuid;
    // reason for the operation
    String motivo;
    
    /**
     * Creates new form Delete
     * 
     * @throws IOException
     */
    public Delete() throws IOException {
        initComponents();
    }
    
    private void initComponents() throws IOException {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();

        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        JPopupMenu menu = new JPopupMenu();
        Action cut = new DefaultEditorKit.CutAction();
        cut.putValue(Action.NAME, "Cut");
        cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
        menu.add( cut );

        Action copy = new DefaultEditorKit.CopyAction();
        copy.putValue(Action.NAME, "Copy");
        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
        menu.add( copy );

        Action paste = new DefaultEditorKit.PasteAction();
        paste.putValue(Action.NAME, "Paste");
        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
        menu.add( paste );

      

        jTextField1.setComponentPopupMenu( menu );
        jTextField2.setComponentPopupMenu( menu );


        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));

        jLabel1.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Preservation Service");

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel2.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel2.setText("Go Back");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel2MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel4.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Delete");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel6.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(jLabel4)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(427, 427, 427))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(30, Short.MAX_VALUE))))
        );

        jPanel2.add(jPanel1);
        jPanel1.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel2.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

       

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel14.setText("Preservation Evidence and the Original Submission");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(330, 70, 510, 20);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel15.setText("Please, enter the UUID to delete the");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(390, 40, 510, 20);


        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel5.setText("Please, enter the reason to this operation");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(360, 220, 510, 20);

        jTextField1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });
        jPanel2.add(jTextField1);
        jTextField1.setBounds(360, 140, 400, 40); //aqui

        jTextField2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField2KeyReleased(evt);
            }
        });
        jPanel2.add(jTextField2);
        jTextField2.setBounds(360, 300, 400, 40); //aqui

        /*jLabel5.setFont(new java.awt.Font("Lucida Grande", 3, 16)); // NOI18N
        jLabel5.setText("Enter UUID of the Preserved Object");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(420, 50, 300, 20);*/

        InputStream del = getClass().getResourceAsStream("/img/del.jpg");
        jButton1.setIcon(new javax.swing.ImageIcon(ImageIO.read(del))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton1ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(490, 375, 140, 50);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(828, 529));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        this.dispose();
        new ManageChoice().setVisible(true);
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18closeFrame
        System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18closeFrame

    // DELETE OPERATION
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) throws UnsupportedEncodingException,ProtocolException,IOException {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        if(uuid== null || uuid == "" || motivo== null || motivo == ""){
          JOptionPane.showMessageDialog(null, "Please, first Enter the UUID and the reason.", "InfoBox: " + "Missing UUID", JOptionPane.INFORMATION_MESSAGE);

        }else{
            // Instantiate the DBAccess service
            DBAccess da = new DBAccess();
            // Get the associated AIP-ID
            String aipId = da.getAipId(uuid);
            if(aipId==""){
                JOptionPane.showMessageDialog(null, "Please, Enter a valid UUID", "InfoBox: " + "INVALID UUID", JOptionPane.INFORMATION_MESSAGE);
                new Delete().setVisible(true);
                this.setVisible(false);
            }else{
                // Delete the uuid entry from database and get a list with AIP-IDs
                java.util.List<String> aips = da.deleteAipUuid(uuid);
                //System.out.println(aips);
                boolean res=false;
                // Instantiate the Roda archival service
                Roda r = new Roda();
                for(String id : aips){
                    // Delete all AIPs from the archival service
                    res = r.deleteAIP(id);
                }
                if(res){
                    // Log message with action and reason
                    LoggerPS log = new LoggerPS();
                    log.logMessage("Preservation Evidences and others deleted by UUID: " + uuid + "Reason: " + motivo);

                    JOptionPane.showMessageDialog(null, "Preservation Evidence and Others deteted with success.", "InfoBox: " + "DELETE", JOptionPane.INFORMATION_MESSAGE);
                    new inicialInterface().setVisible(true);
                    this.setVisible(false);
                }
            }

            

           
          
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    // receive uuid through the preservation interface
    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
                uuid = jTextField1.getText();

    }//GEN-LAST:event_jTextField1KeyReleased


    // receive reason through the preservation interface
    private void jTextField2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
                motivo = jTextField2.getText();

    }//GEN-LAST:event_jTextField1KeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Delete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Delete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Delete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Delete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Delete().setVisible(true);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;

    // End of variables declaration//GEN-END:variables
}
