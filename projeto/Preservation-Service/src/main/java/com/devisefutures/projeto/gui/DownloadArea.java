/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: DownloadArea
*
* This class aims to allow the user to download various objects 
* (e.g. original submission, original submission validation reports, the preservation evidence,
* preservation evidence validation reports and a file with the chosen preservation profile)
*/

package com.devisefutures.projeto.gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


import eu.europa.esig.dss.model.DSSDocument;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class DownloadArea extends javax.swing.JFrame {
    Map<String,DSSDocument> files;
    String home = System.getProperty("user.home");

    /**
     * Creates new form DownloadArea
     * 
     * @throws IOException
     */
    public DownloadArea(Map<String, DSSDocument> f) throws IOException {
        // Object Files received through the class arguments to download
        this.files=f;
        initComponents();
        //List<String> keys = new ArrayList<>(f.keySet());
        
       
        jButton9.setName("0");
        jButton3.setName("1");
        jButton12.setName("2");
        jButton16.setName("3");
        jButton14.setName("4");
        jButton4.setName("5");
        jButton7.setName("6");
        jButton13.setName("11");
        jButton5.setName("7");
        jButton6.setName("8");
        jButton15.setName("9");
        jButton8.setName("10");


        
    }
    
    
    private void initComponents() throws IOException {

        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButton16 = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 23, 0, 0));
        setUndecorated(true);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(0, 102, 153));

        jLabel7.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Preservation Service");

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel9.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel9.setText("Go Back");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel9MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel20.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Download Area");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel21.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel20))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel20)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(416, 416, 416)
                        .addComponent(jLabel8))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(885, 885, 885))
        );

        jPanel5.add(jPanel6);
        jPanel6.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel5.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Calibri", 3, 14)); // NOI18N
        jLabel14.setText("Please, choose the files to download");
        jPanel5.add(jLabel14);
        jLabel14.setBounds(420, 10, 510, 20);

        jLabel10.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel10.setText("Original Submission");
        jPanel5.add(jLabel10);
        jLabel10.setBounds(340, 40, 140, 20);

        jLabel11.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel11.setText("DSS Validation Reports of Preservation Evidence Container");
        jPanel5.add(jLabel11);
        jLabel11.setBounds(380, 410, 370, 30);

        jLabel12.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel12.setText("Preservation Profile");
        jPanel5.add(jLabel12);
        jLabel12.setBounds(560, 290, 240, 20);

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel13.setText("DSS Validation Reports of Original Submission");
        jPanel5.add(jLabel13);
        jLabel13.setBounds(400, 160, 300, 20);

        jLabel15.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel15.setText("Original Submission Signed");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(610, 40, 180, 16);
        
        InputStream sig = getClass().getResourceAsStream("/img/sig.png");
        jButton3.setIcon(new javax.swing.ImageIcon(ImageIO.read(sig))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton3ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton3);
        jButton3.setBounds(680, 70, 60, 70);

        InputStream xml = getClass().getResourceAsStream("/img/xml.png");
        jButton12.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml))); // NOI18N
        jButton12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton12ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton12);
        jButton12.setBounds(330, 190, 60, 60);

        InputStream xml1 = getClass().getResourceAsStream("/img/xml.png");
        jButton13.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml1))); // NOI18N
        jButton13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton13ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton13);
        jButton13.setBounds(670, 320, 60, 60);

        InputStream xml2 = getClass().getResourceAsStream("/img/xml.png");
        jButton14.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml2))); // NOI18N
        jButton14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton14ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton14);
        jButton14.setBounds(610, 190, 60, 60);

        InputStream xml3 = getClass().getResourceAsStream("/img/xml.png");
        jButton4.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml3))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton4ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton4);
        jButton4.setBounds(730, 190, 60, 60);

        InputStream xml4 = getClass().getResourceAsStream("/img/xml.png");
        jButton5.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml4))); // NOI18N
        jButton5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton5ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton5);
        jButton5.setBounds(330, 450, 60, 60);

        InputStream xml5 = getClass().getResourceAsStream("/img/xml.png");
        jButton6.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml5))); // NOI18N
        jButton6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton6ActionPerformed(evt);

                }catch(IOException  e){}
            }
        });
        jPanel5.add(jButton6);
        jButton6.setBounds(460, 450, 60, 60);

        InputStream xml6 = getClass().getResourceAsStream("/img/xml.png");
        jButton15.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml6))); // NOI18N
        jButton15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton15ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton15);
        jButton15.setBounds(610, 450, 60, 60);

        InputStream xml7 = getClass().getResourceAsStream("/img/xml.png");
        jButton8.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml7))); // NOI18N
        jButton8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton8ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton8);
        jButton8.setBounds(730, 450, 60, 60);

        InputStream cont = getClass().getResourceAsStream("/img/cont.png");
        jButton7.setIcon(new javax.swing.ImageIcon(ImageIO.read(cont))); // NOI18N
        jButton7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton7ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton7);
        jButton7.setBounds(380, 320, 70, 60);

        InputStream sig2 = getClass().getResourceAsStream("/img/sig.png");
        jButton9.setIcon(new javax.swing.ImageIcon(ImageIO.read(sig2))); // NOI18N
        jButton9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton9ActionPerformed(evt);
                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton9);
        jButton9.setBounds(370, 70, 60, 70);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel1.setText("AdES LTA");
        jPanel5.add(jLabel1);
        jLabel1.setBounds(690, 140, 50, 20);

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel2.setText("Simple Report");
        jPanel5.add(jLabel2);
        jLabel2.setBounds(320, 520, 70, 20);

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel3.setText("Detailed Report");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(450, 520, 80, 20);

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel4.setText("Diagnostic Data");
        jPanel5.add(jLabel4);
        jLabel4.setBounds(600, 520, 80, 20);

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel5.setText("ETSI Report");
        jPanel5.add(jLabel5);
        jLabel5.setBounds(730, 520, 60, 20);

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel6.setText("Simple Report");
        jPanel5.add(jLabel6);
        jLabel6.setBounds(320, 250, 70, 20);

        jLabel16.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel16.setText("Detailed Report");
        jPanel5.add(jLabel16);
        jLabel16.setBounds(450, 250, 80, 20);

        jLabel17.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel17.setText("Diagnostic Data");
        jPanel5.add(jLabel17);
        jLabel17.setBounds(600, 250, 80, 20);

        jLabel19.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel19.setText("ETSI Report");
        jPanel5.add(jLabel19);
        jLabel19.setBounds(730, 250, 60, 20);


        InputStream xml8 = getClass().getResourceAsStream("/img/xml.png");
        jButton16.setIcon(new javax.swing.ImageIcon(ImageIO.read(xml8))); // NOI18N
        jButton16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton16ActionPerformed(evt);

                }catch(IOException e){}
            }
        });
        jPanel5.add(jButton16);
        jButton16.setBounds(460, 190, 60, 60);

        jLabel22.setFont(new java.awt.Font("Lucida Grande", 3, 12)); // NOI18N
        jLabel22.setText("Preservation Evidence Container ");
        jPanel5.add(jLabel22);
        jLabel22.setBounds(310, 290, 210, 20);

        jLabel23.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel23.setText("ASIC-S XAdES LTA");
        jPanel5.add(jLabel23);
        jLabel23.setBounds(370, 380, 90, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(828, 551));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        this.dispose();
        //VER MELGOR
        new inicialInterface().setVisible(true);
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18closeFrame
        System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18closeFrame

    // DOWNLOAD ACTION (Original Submission)
    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) throws IOException {//GEN-FIRST:event_jButton9ActionPerformed
        DSSDocument f = this.files.get(jButton9.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
        
        
    }//GEN-LAST:event_jButton9ActionPerformed

    // DOWNLOAD ACTION (Original Submission Signed)
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton3ActionPerformed
        DSSDocument f = this.files.get(jButton3.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton3ActionPerformed

    // DOWNLOAD ACTION (Original Submission Simple Report)
    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton12ActionPerformed
        DSSDocument f = this.files.get(jButton12.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton12ActionPerformed

    // DOWNLOAD ACTION (Original Submission Detailed Report)
    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton16ActionPerformed
        DSSDocument f = this.files.get(jButton16.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton16ActionPerformed

    // DOWNLOAD ACTION (Original Submission Diagnostic Data)
    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton14ActionPerformed
        DSSDocument f = this.files.get(jButton14.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton14ActionPerformed

    // DOWNLOAD ACTION (Original Submission ETSI Report)
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton4ActionPerformed
        DSSDocument f = this.files.get(jButton4.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton4ActionPerformed


    // DOWNLOAD ACTION (Preservation Evidence)
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton7ActionPerformed
        DSSDocument f = this.files.get(jButton7.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton7ActionPerformed

    // DOWNLOAD ACTION (Preservation Profile File)
    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton13ActionPerformed
        DSSDocument f = this.files.get(jButton13.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton13ActionPerformed

    // DOWNLOAD ACTION (Preservation Evidence Simple Report)
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton5ActionPerformed
        DSSDocument f = this.files.get(jButton5.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton5ActionPerformed

    // DOWNLOAD ACTION (Preservation Evidence Detailed Report)
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton6ActionPerformed
        DSSDocument f = this.files.get(jButton6.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton6ActionPerformed

    // DOWNLOAD ACTION (Preservation Evidence Diagnostic Data)
    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) throws IOException{//GEN-FIRST:event_jButton15ActionPerformed
        DSSDocument f = this.files.get(jButton15.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton15ActionPerformed

    // DOWNLOAD ACTION (Preservation Evidence ETSI Report)
    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt)throws IOException {//GEN-FIRST:event_jButton8ActionPerformed
        DSSDocument f = this.files.get(jButton8.getName());     
        
        f.save(this.home+"/Downloads/" + f.getName());
        JOptionPane.showMessageDialog(null, "The selected file was transferred to your Download folder.", "InfoBox: " + "File Download", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton8ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    // End of variables declaration//GEN-END:variables
}
