/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: GetEvidence 
*
* This class aims to allow the user who chose the model with storage to perform the action of retrieving objects from the service
* (e.g. he preservation evidence, preservation evidence validation reports, original submission, 
* original submission validation reports and a file with the chosen preservation profile) 
* by collecting the user's unique identifier
*/

package com.devisefutures.projeto.gui;

import com.devisefutures.projeto.*;
import javax.swing.JOptionPane;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;

import eu.europa.esig.dss.model.DSSDocument;

import java.net.ProtocolException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;


public class GetEvidence extends javax.swing.JFrame {
    String uuid;
    
    /**
     * Creates new form GetEvidence
     * 
     * @throws IOException
     */
    public GetEvidence() throws IOException {
        initComponents();
    }
    
    private void initComponents() throws IOException {

        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        JPopupMenu menu = new JPopupMenu();
        Action cut = new DefaultEditorKit.CutAction();
        cut.putValue(Action.NAME, "Cut");
        cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
        menu.add( cut );

        Action copy = new DefaultEditorKit.CopyAction();
        copy.putValue(Action.NAME, "Copy");
        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
        menu.add( copy );

        Action paste = new DefaultEditorKit.PasteAction();
        paste.putValue(Action.NAME, "Paste");
        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
        menu.add( paste );

      

        jTextField1.setComponentPopupMenu( menu );


        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(0, 102, 153));

        jLabel7.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Preservation Service");

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel9.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel9.setText("Go Back");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel9MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel10.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Retrieve");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel2.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addComponent(jLabel10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(427, 427, 427))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53))))
        );

        jPanel5.add(jPanel6);
        jPanel6.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel5.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel14.setText("Preservation Evidence and the Original Submition");
        jPanel5.add(jLabel14);
        jLabel14.setBounds(320, 70, 510, 20);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel15.setText("Please, enter the UUID to get the");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(400, 40, 510, 20);

        jTextField1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });
        jPanel5.add(jTextField1);
        jTextField1.setBounds(340, 260, 420, 40);

        InputStream sub = getClass().getResourceAsStream("/img/sub.jpeg");
        jButton1.setIcon(new javax.swing.ImageIcon(ImageIO.read(sub))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    jButton1ActionPerformed(evt);

                }catch(IOException e){}
                catch(JAXBException j){}
                catch(SAXException j){}
                catch(TransformerConfigurationException j){}
                catch(XMLStreamException j){}
                catch(TransformerException j){}




            }
        });
        jPanel5.add(jButton1);
        jButton1.setBounds(470, 370, 165, 60);

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 3, 16)); // NOI18N
        jLabel5.setText("Enter UUID of the Preserved Object");
        jPanel5.add(jLabel5);
        jLabel5.setBounds(410, 210, 340, 22);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(828, 507));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        this.dispose();
        new ManageChoice().setVisible(true);
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18closeFrame
        System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18closeFrame

    // Get th user identifier
    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
        uuid = jTextField1.getText();
    }//GEN-LAST:event_jTextField1KeyReleased

    // Prepare the files to download
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) throws TransformerException,IOException,JAXBException,SAXException,TransformerConfigurationException,XMLStreamException,ProtocolException,IOException{//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //uuid = jTextField1.getText();

        if(uuid== null || uuid == ""){
            JOptionPane.showMessageDialog(null, "Please, first Enter the UUID", "InfoBox: " + "Missing UUID", JOptionPane.INFORMATION_MESSAGE);

        }else{
            // Instantiate the DBAccess service
            DBAccess da = new DBAccess();
            // Get the associated AIP-ID
            String aipId = da.getAipId(uuid);
            
            // Instantiate th Loading Animation object
            LoadingGet lo = new LoadingGet();
            if(aipId==""){
                JOptionPane.showMessageDialog(null, "Please, Enter a valid UUID", "InfoBox: " + "INVALID UUID", JOptionPane.INFORMATION_MESSAGE);
                new GetEvidence().setVisible(true);
                this.setVisible(false);
            }else{
                JOptionPane.showMessageDialog(null, "This operation may take a while.", "InfoBox: " + "Creating Preservation Evidences", JOptionPane.INFORMATION_MESSAGE);
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            // Instantiate the Roda archival service
                            Roda r = new Roda();
                            // Preservation Evidence retrieved from previous step (ASiC container)
                            DSSDocument cont = r.getFileFromAip(aipId);
                            // Instantiate the Container Validation service
                            ContainerValAndCont cv = new ContainerValAndCont(cont);
                            // Container's content
                            java.util.List<DSSDocument> l = cv.retrievedOriginalDocuments;

                            // Instantiate the Map object to add the files to download
                            Map<String,DSSDocument> res = new HashMap<String,DSSDocument>();
                            // Original Submission
                            res.put("0",l.get(0));
                            // Original Submission Signed
                            res.put("1",l.get(1));
                            // Original Submission Simple Report
                            res.put("2",l.get(2));
                            // Original Submission Detailed Report
                            res.put("3",l.get(3));
                            // Original Submission Diagnostic Data
                            res.put("4",l.get(4));
                            // Original Submission ETSI Report
                            res.put("5",l.get(5));
                            // Preservation Evidence
                            res.put("6",cont);
                            // Preservation Evidence Simple Report
                            res.put("7",cv.splReport);
                            // Preservation Evidence Detailed Report
                            res.put("8",cv.detailReport);
                            // Preservation Evidence Diagnostic Data 
                            res.put("9",cv.diagoReport);
                            // Preservation Evidence ETSI Report
                            res.put("10",cv.etsiReport);

                            // Expected evidence duration
                            Date max = cv.simpleReport.getSignatureExtensionPeriodMax(cv.simpleReport.getFirstSignatureId());
                            // Preservation Profile File
                            WriteXMLFile xml = new WriteXMLFile();
                            DSSDocument pol = xml.createProfileFile("WITHOUT_STORAGE", "LONG-TERM PRESERVATION", max);
                            res.put("11", pol);

                            // Log message with action 
                            LoggerPS log = new LoggerPS();
                            log.logMessage("Preservation Evidences and others requested by UUID: " + uuid);
                            // Instantiate Download Area for user
                            new DownloadArea(res).setVisible(true);  
                            lo.setVisible(false);          
                         
                        }catch(IOException e){}
                        catch(JAXBException e){}
                        catch(SAXException e){}
                        catch(XMLStreamException e){}
                        catch(TransformerException e){}
                    }
                });  
                lo.setVisible(true);
    
                t1.start();
                this.setVisible(false);
    
                

            }

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GetEvidence.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GetEvidence.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GetEvidence.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GetEvidence.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new GetEvidence().setVisible(true);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
