/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: UpdateSub 
*
* This class aims to give the user who chose the preservation profile with storage the possibility 
* to perform the operation of replacing the original submission to the Proof of Concept system by collecting the user's 
* unique identifier and the new signature file
*/

package com.devisefutures.projeto.gui;

import com.devisefutures.projeto.*;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.MimeType;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.io.UnsupportedEncodingException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;

public class UpdateSub extends javax.swing.JFrame {
    JFileChooser fc = null;
    File update = null;
    String uuid = null;
    DSSDocument signature = null;

    /**
     * Creates new form UpdateSub
     * 
     * @throws IOException
     */
    public UpdateSub() throws IOException {
        initComponents();
    }

    private void initComponents() throws IOException {

        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        JPopupMenu menu = new JPopupMenu();
        Action cut = new DefaultEditorKit.CutAction();
        cut.putValue(Action.NAME, "Cut");
        cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
        menu.add(cut);

        Action copy = new DefaultEditorKit.CopyAction();
        copy.putValue(Action.NAME, "Copy");
        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
        menu.add(copy);

        Action paste = new DefaultEditorKit.PasteAction();
        paste.putValue(Action.NAME, "Paste");
        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
        menu.add(paste);

        jTextField1.setComponentPopupMenu(menu);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        jPanel4.setBackground(new java.awt.Color(0, 102, 153));

        jLabel4.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Preservation Service");

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel6.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel6.setText("Go Back");
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel6MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel7.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Update Original");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel9.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup().addGroup(jPanel4Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup().addGap(97, 97, 97).addComponent(jLabel5)
                                .addGap(18, 18, 18).addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 282,
                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createSequentialGroup().addGap(14, 14, 14)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4).addComponent(jLabel6,
                                                javax.swing.GroupLayout.PREFERRED_SIZE, 95,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel4Layout.createSequentialGroup().addGap(41, 41, 41).addComponent(jLabel7)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 35,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(
                                        jPanel4Layout.createSequentialGroup().addGap(80, 80, 80).addComponent(jLabel5))
                                .addGroup(jPanel4Layout.createSequentialGroup().addGap(27, 27, 27).addComponent(jLabel9,
                                        javax.swing.GroupLayout.PREFERRED_SIZE, 398,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(40, 40, 40)));

        jPanel3.add(jPanel4);
        jPanel4.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel3.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel14.setText("the new  Submission Object");
        jPanel3.add(jLabel14);
        jLabel14.setBounds(430, 50, 510, 20);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel15.setText("Please, enter the your UUID and upload");
        jPanel3.add(jLabel15);
        jLabel15.setBounds(380, 20, 510, 20);

        InputStream sub = getClass().getResourceAsStream("/img/sub.jpeg");
        jButton3.setIcon(new javax.swing.ImageIcon(ImageIO.read(sub))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButton3ActionPerformed(evt);

                } catch (IOException e) {
                } catch (JAXBException e) {
                } catch (XMLStreamException e) {
                } catch (TransformerException e) {
                } catch (SAXException e) {
                } catch (InterruptedException e) {
                }

            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(470, 450, 160, 60);

        jTextField1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });
        jPanel3.add(jTextField1);
        jTextField1.setBounds(350, 150, 410, 40);

        InputStream assi = getClass().getResourceAsStream("/img/assi.png");
        jButton4.setIcon(new javax.swing.ImageIcon(ImageIO.read(assi))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton4);
        jButton4.setBounds(500, 220, 100, 100);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel1.setText("New ESig or ESeal Upload");
        jPanel3.add(jLabel1);
        jLabel1.setBounds(470, 330, 180, 20);

        jLabel2.setText("  ");
        jPanel3.add(jLabel2);
        jLabel2.setBounds(390, 360, 360, 30);

        jLabel16.setText("  ");
        jPanel3.add(jLabel16);
        jLabel16.setBounds(390, 390, 360, 30);

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 3, 16)); // NOI18N
        jLabel13.setText("Enter UUID of the Preserved Object");
        jPanel3.add(jLabel13);
        jLabel13.setBounds(410, 120, 300, 22);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addComponent(jPanel3,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)));

        setSize(new java.awt.Dimension(828, 529));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel6MouseClicked
        // TODO add your handling code here:
        this.dispose();
        new ManageChoice().setVisible(true);
    }// GEN-LAST:event_jLabel6MouseClicked

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jLabel18closeFrame
        System.exit(0); // TODO add your handling code here:
    }// GEN-LAST:event_jLabel18closeFrame

    // Do the Update Operation
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
            throws IOException, JAXBException, SAXException, XMLStreamException, UnsupportedEncodingException,
            ProtocolException, IOException, InterruptedException, TransformerException {// GEN-FIRST:event_jButton3ActionPerformed
        if (update == null || uuid == null || signature == null) {
            JOptionPane.showMessageDialog(null, "Please, first Enter the UUID and upload the new file",
                    "InfoBox: " + "Missing UUID/New file", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "This may take a while", "InfoBox: " + "UPDATE",
                    JOptionPane.INFORMATION_MESSAGE);
            // Instantiate the Loading Animation object
            Loading l = new Loading();

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Instantiate the UpdateOriginalSub service
                        ServiceUpdateOriginalSub up = new ServiceUpdateOriginalSub();
                        // Make the update with the received uuid and new signature file
                        boolean res = up.updateOriginal(uuid, signature);
                        // if update successful (res!=0)
                        if (res) {
                            // Log message with action
                            LoggerPS log = new LoggerPS();
                            log.logMessage("Original Submission Update for UUID: " + uuid
                                    + " New Original Submission File: " + signature.getName());

                            JOptionPane.showMessageDialog(null, "Object Updated", "InfoBox: " + "UPDATE",
                                    JOptionPane.INFORMATION_MESSAGE);
                            // Instantiate initialInterface for user
                            new inicialInterface().setVisible(true);
                            l.setVisible(false);
                            // this.setVisible(false);
                        } else {
                            JOptionPane.showMessageDialog(null, "Invalid UUID", "InfoBox: " + "INVALID UUID",
                                    JOptionPane.INFORMATION_MESSAGE);
                            new UpdateSub().setVisible(true);
                            l.setVisible(false);

                        }
                    } catch (IOException e) {
                    } catch (JAXBException e) {
                    } catch (SAXException e) {
                    } catch (XMLStreamException e) {
                    } catch (InterruptedException e) {
                    } catch (TransformerException e) {
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });  
            l.setVisible(true);

            t1.start();
            this.setVisible(false);
        
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    // Get the user's uuid
    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
        uuid = jTextField1.getText();
    }//GEN-LAST:event_jTextField1KeyReleased

    // Get the new signature file
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        fc = new JFileChooser();

        int returnVal = fc.showOpenDialog(UpdateSub.this);

        if(returnVal == JFileChooser.APPROVE_OPTION){
            update = fc.getSelectedFile();
            signature = new FileDocument(update);
            if(signature.getMimeType().equals(MimeType.XML) || signature.getMimeType().equals(MimeType.PDF) || signature.getMimeType().equals(MimeType.PKCS7) || signature.getMimeType().equals(MimeType.ASICS)){

                jLabel2.setText(update.getName());
                jLabel16.setText("Uploaded with success");
    
            }
            else{
                update=null;
                JOptionPane.showMessageDialog(null, "this software is a prototype, only accepts AdES Signatures with .xml, .pdf, .scs...","Preservation Service Without Storage" ,JOptionPane.INFORMATION_MESSAGE);

            }

        }else{
            jLabel2.setText("Upload canceled by the User");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateSub.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateSub.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateSub.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateSub.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new UpdateSub().setVisible(true);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
