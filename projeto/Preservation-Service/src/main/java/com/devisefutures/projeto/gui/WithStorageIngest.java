/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: WithStorageIngest 
*
* This class aims to initiate the process of ingesting the original submission to create preservation evidence 
* for users who choose the preservation profile with storage
*/

package com.devisefutures.projeto.gui;

import com.devisefutures.projeto.*;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.MimeType;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.io.UnsupportedEncodingException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;

public class WithStorageIngest extends javax.swing.JFrame {
    JFileChooser fc;
    File sig = null;
    DSSDocument signature = null;
    String uuid = null;

    /**
     * Creates new form WithStorageIngest
     * 
     * @throws IOException
     */
    public WithStorageIngest() throws IOException {
        initComponents();
    }

    private void initComponents() throws IOException {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 23, 0, 0));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(828, 610));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));

        jLabel1.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Preservation Service");

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel2.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel2.setText("Go Back");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel2MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel4.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("With Storage");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel5.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup().addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout
                                                .createSequentialGroup().addGap(77, 77, 77).addComponent(jLabel3))
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup().addGap(113, 113, 113).addComponent(jLabel5,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(jLabel1))
                        .addGroup(jPanel1Layout.createSequentialGroup().addGap(53, 53, 53).addComponent(jLabel4)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 36,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 398,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3).addGap(29, 29, 29)));

        jPanel2.add(jPanel1);
        jPanel1.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel2.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

        InputStream assi = getClass().getResourceAsStream("/img/assi.png");
        jButton1.setIcon(new javax.swing.ImageIcon(ImageIO.read(assi))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(500, 160, 100, 100);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel15.setText("Please, upload the signature file");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(400, 60, 510, 20);

        InputStream sub = getClass().getResourceAsStream("/img/sub.jpeg");
        jButton3.setIcon(new javax.swing.ImageIcon(ImageIO.read(sub))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButton3ActionPerformed(evt);
                } catch (IOException ex) {
                } catch (JAXBException ex) {
                } catch (SAXException ex) {
                } catch (InterruptedException ex) {
                } catch (TransformerException ex) {
                } catch (XMLStreamException ex) {
                }

            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(470, 420, 160, 60);

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel6.setText("ESig or ESeal Upload");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(480, 270, 140, 16);

        jLabel8.setFont(new java.awt.Font("Lucida Grande", 2, 12)); // NOI18N
        jLabel8.setText(" ");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(420, 310, 330, 30);

        jLabel11.setFont(new java.awt.Font("Lucida Grande", 2, 12)); // NOI18N
        jLabel11.setText(" ");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(420, 340, 330, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addComponent(jPanel2,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)));

        setSize(new java.awt.Dimension(828, 529));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Ingest Process
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
            throws IOException, JAXBException, SAXException, XMLStreamException, UnsupportedEncodingException,
            ProtocolException, IOException, InterruptedException, TransformerException {// GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:

        if (sig == null || signature == null) {
            JOptionPane.showMessageDialog(null, "Please, first upload the files above", "InfoBox: " + "Missing Files",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "This operation may take a while.",
                    "InfoBox: " + "Creating Preservation Evidences", JOptionPane.INFORMATION_MESSAGE);
            Loading l = new Loading();

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Instantiate the ServiceInit Service
                        ServiceInit si = new ServiceInit();
                        // InitService with Storage was chosen (the uuid is to return to the user)
                        uuid = si.initServiceWithStorage(signature);

                        // Log message with action
                        LoggerPS log = new LoggerPS();
                        log.logMessage("Preservation With Storage was choosen to the file: " + signature.getName()
                                + "  PSOutput UUID: " + uuid);

                        // Return uuid to the user
                        javax.swing.JTextArea ta = new javax.swing.JTextArea();
                        ta.setText("YOUR UUID:  " + uuid);
                        ta.setEditable(false);
                        ta.setBackground(java.awt.Color.LIGHT_GRAY);

                        JPopupMenu menu = new JPopupMenu();
                        Action cut = new DefaultEditorKit.CutAction();
                        cut.putValue(Action.NAME, "Cut");
                        cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
                        menu.add(cut);

                        Action copy = new DefaultEditorKit.CopyAction();
                        copy.putValue(Action.NAME, "Copy");
                        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
                        menu.add(copy);

                        Action paste = new DefaultEditorKit.PasteAction();
                        paste.putValue(Action.NAME, "Paste");
                        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
                        menu.add(paste);

                        ta.setComponentPopupMenu(menu);

                        JOptionPane.showMessageDialog(null, ta, "InfoBox: " + "Save this UUID to later manage",
                                JOptionPane.INFORMATION_MESSAGE);
                        new inicialInterface().setVisible(true);
                        l.setVisible(false);

                    } catch (IOException e) {
                    } catch (JAXBException e) {
                    } catch (SAXException e) {
                    } catch (XMLStreamException e) {
                    } catch (InterruptedException e) {
                    } catch (TransformerException e) {
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });  
            l.setVisible(true);

            t1.start();
            this.setVisible(false);
            
            
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    // Get the Signature File
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        fc = new JFileChooser();

        int returnVal = fc.showOpenDialog(WithStorageIngest.this);

        if(returnVal == JFileChooser.APPROVE_OPTION){
            sig = fc.getSelectedFile();
            signature = new FileDocument(sig);
            if(signature.getMimeType().equals(MimeType.XML) || signature.getMimeType().equals(MimeType.PDF) || signature.getMimeType().equals(MimeType.PKCS7) || signature.getMimeType().equals(MimeType.ASICS)){

                jLabel8.setText(sig.getName());
                jLabel11.setText("Uploaded with success");
    
            }
            else{
                sig=null;
                JOptionPane.showMessageDialog(null, "this software is a prototype, only accepts AdES Signatures with .xml, .pdf, .scs...","Preservation Service Without Storage" ,JOptionPane.INFORMATION_MESSAGE);

            }
        }else{
            jLabel8.setText("Upload canceled by the User");
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18closeFrame
        System.exit(0);    // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18closeFrame

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        this.dispose();
        new ChoiceStorage().setVisible(true);
    }//GEN-LAST:event_jLabel2MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WithStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WithStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WithStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WithStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new WithStorageIngest().setVisible(true);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
