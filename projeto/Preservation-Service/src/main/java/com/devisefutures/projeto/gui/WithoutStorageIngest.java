/*
* Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service
* It takes into account the eIDAS Regulation (EU Regulation Nº 910/2014) 
* and associated standards (e.g. ETSI 119 511 and ETSI 119 512)
*
* This project was carried out as part of the Master's thesis in Informatics Engineering
* under the name "eIDAS Qualified Trust Services - Preservation Service".
*
*
* Developed by João M. Fernandes - pg38930@alunos.uminho.pt
*
*
* This file is part of the Preservation Interface of the "Proof of Concept of an Electronic Signature and Electronic Seal Preservation Service" project
*
* Class Name: WithoutStorageIngest 
*
* This class aims to initiate the process of ingesting the original submission to create preservation evidence 
* for users who choose the preservation profile without storage
*/

package com.devisefutures.projeto.gui;

import com.devisefutures.projeto.*;
import java.io.File;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.MimeType;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import javax.xml.bind.JAXBException;
import java.util.Date;

public class WithoutStorageIngest extends javax.swing.JFrame {
    JFileChooser fc;
    File sig = null;
    DSSDocument signature = null;

    /**
     * Creates new form WithoutStorage
     * 
     * @throws IOException
     */
    public WithoutStorageIngest() throws IOException {
        initComponents();

    }

    private void initComponents() throws IOException {

        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 23, 0, 0));
        setUndecorated(true);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jPanel6.setBackground(new java.awt.Color(0, 102, 153));

        jLabel7.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Preservation Service");

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 0, 51));
        InputStream atras = getClass().getResourceAsStream("/img/atras.png");
        jLabel9.setIcon(new javax.swing.ImageIcon(ImageIO.read(atras))); // NOI18N
        jLabel9.setText("Go Back");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    jLabel9MouseClicked(evt);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        jLabel10.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Without Storage");

        InputStream mark = getClass().getResourceAsStream("/img/mark.png");
        jLabel2.setIcon(new javax.swing.ImageIcon(ImageIO.read(mark))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                        jPanel6Layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 282,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(76, 76, 76))
                .addGroup(jPanel6Layout.createSequentialGroup().addGroup(jPanel6Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup().addGap(463, 463, 463).addComponent(jLabel8))
                        .addGroup(jPanel6Layout.createSequentialGroup().addGap(14, 14, 14)
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 94,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel7)))
                        .addGroup(jPanel6Layout.createSequentialGroup().addGap(41, 41, 41).addComponent(jLabel10)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                        jPanel6Layout.createSequentialGroup().addContainerGap()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 38,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8).addGap(22, 22, 22).addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10).addGap(18, 18, 18)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 398,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(19, 19, 19)));

        jPanel5.add(jPanel6);
        jPanel6.setBounds(0, 0, 277, 560);

        InputStream fechar = getClass().getResourceAsStream("/img/fechar.png");
        jLabel18.setIcon(new javax.swing.ImageIcon(ImageIO.read(fechar))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel18closeFrame(evt);
            }
        });
        jPanel5.add(jLabel18);
        jLabel18.setBounds(810, 0, 15, 20);

        InputStream assi = getClass().getResourceAsStream("/img/assi.png");
        jButton1.setIcon(new javax.swing.ImageIcon(ImageIO.read(assi))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1);
        jButton1.setBounds(500, 160, 100, 100);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel15.setText("Please, upload the signature file");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(400, 60, 510, 20);

        InputStream sub = getClass().getResourceAsStream("/img/sub.jpeg");
        jButton3.setIcon(new javax.swing.ImageIcon(ImageIO.read(sub))); // NOI18N
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jButton3ActionPerformed(evt);
                } catch (IOException ex) {
                } catch (JAXBException ex) {
                } catch (SAXException ex) {
                } catch (InterruptedException ex) {
                } catch (TransformerException ex) {
                } catch (XMLStreamException ex) {
                }

            }
        });
        jPanel5.add(jButton3);
        jButton3.setBounds(470, 440, 170, 60);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Lucida Grande", 2, 12)); // NOI18N
        jLabel4.setText(" ");
        jPanel5.add(jLabel4);
        jLabel4.setBounds(420, 310, 330, 30);

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 2, 12)); // NOI18N
        jLabel6.setText(" ");
        jPanel5.add(jLabel6);
        jLabel6.setBounds(420, 340, 330, 30);

        jLabel12.setFont(new java.awt.Font("Lucida Grande", 3, 13)); // NOI18N
        jLabel12.setText("ESig or ESeal Upload");
        jPanel5.add(jLabel12);
        jLabel12.setBounds(480, 270, 140, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addComponent(jPanel5,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)));

        setSize(new java.awt.Dimension(828, 551));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) throws IOException {// GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        this.dispose();
        new ChoiceStorage().setVisible(true);
    }// GEN-LAST:event_jLabel9MouseClicked

    private void jLabel18closeFrame(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jLabel18closeFrame
        System.exit(0); // TODO add your handling code here:
    }// GEN-LAST:event_jLabel18closeFrame

    // Get the signature file
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        fc = new JFileChooser();

        int returnVal = fc.showOpenDialog(WithoutStorageIngest.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            sig = fc.getSelectedFile();
            signature = new FileDocument(sig);
            if (signature.getMimeType().equals(MimeType.XML) || signature.getMimeType().equals(MimeType.PDF)
                    || signature.getMimeType().equals(MimeType.PKCS7)
                    || signature.getMimeType().equals(MimeType.ASICS)) {

                jLabel4.setText(sig.getName());
                jLabel6.setText("Uploaded with success");

            } else {
                sig = null;
                JOptionPane.showMessageDialog(null,
                        "this software is a prototype, only accepts AdES Signatures with .xml, .pdf, .scs...",
                        "Preservation Service Without Storage", JOptionPane.INFORMATION_MESSAGE);

            }

        } else {
            jLabel4.setText("Upload canceled by the User");
        }
    }// GEN-LAST:event_jButton1ActionPerformed

    // Ingest process
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
            throws IOException, JAXBException, SAXException, InterruptedException, TransformerConfigurationException,
            XMLStreamException, TransformerException {// GEN-FIRST:event_jButton3ActionPerformed{//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:

        // CHAMAR AS OUTRA E DEVOLVER O ID

        if (signature == null || sig == null) {
            JOptionPane.showMessageDialog(null, "Please, first Upload the requiered files above",
                    "InfoBox: " + "Missing Files", JOptionPane.INFORMATION_MESSAGE);
            // jTextField4.setText("First Upload the requiered files above");
        } else {
            JOptionPane.showMessageDialog(null, "This operation may take a while.",
                    "InfoBox: " + "Creating Preservation Evidences", JOptionPane.INFORMATION_MESSAGE);
            Loading l = new Loading();

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Instantiate the ServiceInit Service
                        ServiceInit sv = new ServiceInit();

                        // ServiceInit without Storage was chosen (The Map object contains the
                        // preservation evidence)
                        Map<String, DSSDocument> out = sv.serviceInitWithoutStorage(signature);

                        // Expected preservation evidence duration
                        Date max = sv.max;

                        // Log message with action
                        LoggerPS log = new LoggerPS();
                        log.logMessage("Preservation Without Storage was choosen to the file: " + signature.getName());
                        JOptionPane.showMessageDialog(null,
                                "This is a date calculated on the basis of the mathematical validity of the cryptographic algorithms\nused to construct the preservation evidence.It does not take into account external events (e.g. revocation of certificates).\nExtend your preservation evidence before: "
                                        + max,
                                "InfoBox: " + "Expected Evidence Duration", JOptionPane.INFORMATION_MESSAGE);

                        // Preservation Profile File
                        WriteXMLFile xml = new WriteXMLFile();
                        DSSDocument pol = xml.createProfileFile("WITHOUT_STORAGE", "LONG-TERM PRESERVATION", max);
                        out.put("11", pol);

                        // Instantiate Download Area for user
                        new DownloadArea(out).setVisible(true);
                        l.setVisible(false);
                    } catch (IOException e) {
                    } catch (JAXBException e) {
                    } catch (SAXException e) {
                    } catch (XMLStreamException e) {
                    } catch (InterruptedException e) {
                    } catch (TransformerException e) {
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
  
            l.setVisible(true);

            t1.start();
            this.setVisible(false);
            


        }
        
        
      
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WithoutStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WithoutStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WithoutStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WithoutStorageIngest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new WithoutStorageIngest().setVisible(true);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    



    // End of variables declaration//GEN-END:variables
}
